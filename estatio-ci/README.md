# Official Jenkins Docker image

# Usage

Build image:
```
docker build -t simplew/estatio-ci:0.1.0 --file Dockerfile .
```

Run image:
```
docker run --name estatio-ci -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock simplew/estatio-ci:0.1.0
```

Jenkins credentials:
```
user: test
password: test
```

Create estatio-pr build job:
```
java -jar jenkins-cli.jar -s http://localhost:8080/ -auth test:test create-job estatio-pr < jenkins-files/estatio-pr-job.xml
```
